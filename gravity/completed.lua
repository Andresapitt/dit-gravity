-----------------------------------------------------------------------------------------------------------------------------

local storyboard 		= require( "storyboard" )
-- include Corona's "widget" library
local widget = require "widget"
local scene 			= storyboard.newScene()


local image, againBtn, menuBtn


-- 'onRelease' event listener for againBtn
local function onPlayBtnRelease()
	-- go to level1.lua scene
	storyboard.gotoScene( "level1", "fade", 500 )
	return true	-- indicates successful touch
end

-- 'onRelease' event listener for menuBtn
local function onMenuBtnRelease()
	-- go to level1.lua scene
	storyboard.gotoScene( "menu", "fade", 500 )
	return true	-- indicates successful touch
end



-- Called when the scene's view does not exist:
function scene:createScene( event )
	local screenGroup = self.view

	local background = display.newRect( 0, 0, _G._w , _G._h )
	  background:setFillColor( black )
	
	image = display.newImageRect( "images/backToEarth.png",250,167 )
	image.x = display.contentWidth/2
	image.y = display.contentHeight/2

	againBtn = widget.newButton{
		label="Play Again",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		font="Lintel",
		fontSize=15,
		width=154, height=40,
		onRelease = onPlayBtnRelease	-- event listener function
	}
	againBtn:setReferencePoint( display.CenterRightReferencePoint )
	againBtn.x = display.contentWidth*0.5 - againBtn.width/2 + 50
	againBtn.y = display.contentHeight - 100

	-- create a widget button (which will loads level1.lua on release)
	menuBtn = widget.newButton{
		label="Back to Menu",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		font="Lintel",
		fontSize=15,
		width=154, height=40,
		onRelease = onMenuBtnRelease	-- event listener function
	}
	menuBtn:setReferencePoint( display.CenterLeftReferencePoint )
	menuBtn.x = display.contentWidth*0.5 + menuBtn.width/2 - 50
	menuBtn.y = display.contentHeight - 100


	local gameOverText= display.newText( "Mission Completed", 0,0, "Lintel", 20 )
	gameOverText:setReferencePoint( display.CenterReferencePoint )
	gameOverText.x, gameOverText.y = display.contentWidth/2, display.contentHeight/6

	local scoreText= display.newText( "Final Score: ".._G.gameScore, 0,0, "Lintel", 17 )
	scoreText:setReferencePoint( display.CenterReferencePoint )
	scoreText.x, scoreText.y = display.contentWidth/2, gameOverText.y + gameOverText.height + 5

	screenGroup:insert( background )
	screenGroup:insert( image )
	screenGroup:insert(gameOverText)
	screenGroup:insert( scoreText)
	screenGroup:insert(againBtn)
	screenGroup:insert(menuBtn)
end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )

end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
		

end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	
	local group = self.view
	
	print( "((destroying completed 's view))" )
	
	if againBtn then
		againBtn:removeSelf()	-- widgets must be manually removed
		againBtn = nil
	end

	if menuBtn then
		menuBtn:removeSelf()	-- widgets must be manually removed
		menuBtn = nil
	end
end


function scene:didExitScene( event )
storyboard.purgeScene( "failed" )
storyboard.removeScene( "failed" )
end

scene:addEventListener( "didExitScene" )

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene