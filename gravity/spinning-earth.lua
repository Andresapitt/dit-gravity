--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:8a5e7f5558f6e53642a3c698d3a3b901:6502da88cb8f4c0c2272b28ce0abf9e8:df7780d118beda49beae761b0439c2c0$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- earth_00
            x=458,
            y=1922,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_01
            x=2,
            y=1922,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_02
            x=458,
            y=1842,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_03
            x=2,
            y=1842,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_04
            x=458,
            y=1762,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_05
            x=2,
            y=1762,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_06
            x=458,
            y=1682,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_07
            x=2,
            y=1682,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_08
            x=458,
            y=1602,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_09
            x=2,
            y=1602,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_10
            x=458,
            y=1522,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_11
            x=2,
            y=1522,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_12
            x=458,
            y=1442,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_13
            x=2,
            y=1442,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_14
            x=458,
            y=1362,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_15
            x=2,
            y=1362,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_16
            x=458,
            y=1282,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_17
            x=2,
            y=1282,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_18
            x=458,
            y=1202,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_19
            x=2,
            y=1202,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_20
            x=458,
            y=1122,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_21
            x=2,
            y=1122,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_22
            x=458,
            y=1042,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_23
            x=2,
            y=1042,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_24
            x=458,
            y=962,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_25
            x=2,
            y=962,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_26
            x=458,
            y=882,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_27
            x=2,
            y=882,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_28
            x=458,
            y=802,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_29
            x=2,
            y=802,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_30
            x=458,
            y=722,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_31
            x=2,
            y=722,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_32
            x=458,
            y=642,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_33
            x=2,
            y=642,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_34
            x=458,
            y=562,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_35
            x=2,
            y=562,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_36
            x=458,
            y=482,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_37
            x=2,
            y=482,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_38
            x=458,
            y=402,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_39
            x=2,
            y=402,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_40
            x=458,
            y=322,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_41
            x=2,
            y=322,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_42
            x=458,
            y=242,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_43
            x=2,
            y=242,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_44
            x=458,
            y=162,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_45
            x=2,
            y=162,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_46
            x=458,
            y=82,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_47
            x=2,
            y=82,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_48
            x=458,
            y=2,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
        {
            -- earth_49
            x=2,
            y=2,
            width=454,
            height=78,

            sourceX = 21,
            sourceY = 12,
            sourceWidth = 480,
            sourceHeight = 90
        },
    },
    
    sheetContentWidth = 914,
    sheetContentHeight = 2002
}

SheetInfo.frameIndex =
{

    ["earth_00"] = 1,
    ["earth_01"] = 2,
    ["earth_02"] = 3,
    ["earth_03"] = 4,
    ["earth_04"] = 5,
    ["earth_05"] = 6,
    ["earth_06"] = 7,
    ["earth_07"] = 8,
    ["earth_08"] = 9,
    ["earth_09"] = 10,
    ["earth_10"] = 11,
    ["earth_11"] = 12,
    ["earth_12"] = 13,
    ["earth_13"] = 14,
    ["earth_14"] = 15,
    ["earth_15"] = 16,
    ["earth_16"] = 17,
    ["earth_17"] = 18,
    ["earth_18"] = 19,
    ["earth_19"] = 20,
    ["earth_20"] = 21,
    ["earth_21"] = 22,
    ["earth_22"] = 23,
    ["earth_23"] = 24,
    ["earth_24"] = 25,
    ["earth_25"] = 26,
    ["earth_26"] = 27,
    ["earth_27"] = 28,
    ["earth_28"] = 29,
    ["earth_29"] = 30,
    ["earth_30"] = 31,
    ["earth_31"] = 32,
    ["earth_32"] = 33,
    ["earth_33"] = 34,
    ["earth_34"] = 35,
    ["earth_35"] = 36,
    ["earth_36"] = 37,
    ["earth_37"] = 38,
    ["earth_38"] = 39,
    ["earth_39"] = 40,
    ["earth_40"] = 41,
    ["earth_41"] = 42,
    ["earth_42"] = 43,
    ["earth_43"] = 44,
    ["earth_44"] = 45,
    ["earth_45"] = 46,
    ["earth_46"] = 47,
    ["earth_47"] = 48,
    ["earth_48"] = 49,
    ["earth_49"] = 50,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
