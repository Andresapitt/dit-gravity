--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:19154db522a20c6584a4f94bf734ff06:589fed5c4952e92c99758ba7afb08e5d:3228e5c33a2f8e8563ac8fce1c9e1c8f$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- camera
            x=148,
            y=2,
            width=35,
            height=29,

        },
        {
            -- disc
            x=111,
            y=2,
            width=35,
            height=38,

        },
        {
            -- drill
            x=2,
            y=2,
            width=35,
            height=50,

        },
        {
            -- hammer
            x=77,
            y=2,
            width=32,
            height=39,

        },
        {
            -- hinges
            x=39,
            y=2,
            width=36,
            height=45,

        },
    },
    
    sheetContentWidth = 185,
    sheetContentHeight = 54
}

SheetInfo.frameIndex =
{

    ["camera"] = 1,
    ["disc"] = 2,
    ["drill"] = 3,
    ["hammer"] = 4,
    ["hinges"] = 5,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
