--
--  HUD objects to collect
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- camera
            x=2,
            y=299,
            width=20,
            height=19,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 20,
            sourceHeight = 29
        },
        {
            -- disc
            x=2,
            y=232,
            width=20,
            height=23,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 20,
            sourceHeight = 29
        },
        {
            -- drill
            x=2,
            y=151,
            width=20,
            height=29,

        },
        {
            -- hammer
            x=2,
            y=370,
            width=19,
            height=23,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 19,
            sourceHeight = 29
        },
        {
            -- hinges
            x=2,
            y=60,
            width=21,
            height=27,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 21,
            sourceHeight = 29
        },
        {
            -- toCollect_camera
            x=2,
            y=278,
            width=20,
            height=19,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 20,
            sourceHeight = 29
        },
        {
            -- toCollect_camera_red
            x=2,
            y=257,
            width=20,
            height=19,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 20,
            sourceHeight = 29
        },
        {
            -- toCollect_disc
            x=2,
            y=207,
            width=20,
            height=23,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 20,
            sourceHeight = 29
        },
        {
            -- toCollect_disc_red
            x=2,
            y=182,
            width=20,
            height=23,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 20,
            sourceHeight = 29
        },
        {
            -- toCollect_drill
            x=2,
            y=120,
            width=20,
            height=29,

        },
        {
            -- toCollect_drill_red
            x=2,
            y=89,
            width=20,
            height=29,

        },
        {
            -- toCollect_hammer
            x=2,
            y=345,
            width=19,
            height=23,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 19,
            sourceHeight = 29
        },
        {
            -- toCollect_hammer_red
            x=2,
            y=320,
            width=19,
            height=23,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 19,
            sourceHeight = 29
        },
        {
            -- toCollect_hinges
            x=2,
            y=31,
            width=21,
            height=27,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 21,
            sourceHeight = 29
        },
        {
            -- toCollect_hinges_red
            x=2,
            y=2,
            width=21,
            height=27,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 21,
            sourceHeight = 29
        },
    },
    
    sheetContentWidth = 25,
    sheetContentHeight = 395
}

SheetInfo.frameIndex =
{

    ["camera"] = 1,
    ["disc"] = 2,
    ["drill"] = 3,
    ["hammer"] = 4,
    ["hinges"] = 5,
    ["toCollect_camera"] = 6,
    ["toCollect_camera_red"] = 7,
    ["toCollect_disc"] = 8,
    ["toCollect_disc_red"] = 9,
    ["toCollect_drill"] = 10,
    ["toCollect_drill_red"] = 11,
    ["toCollect_hammer"] = 12,
    ["toCollect_hammer_red"] = 13,
    ["toCollect_hinges"] = 14,
    ["toCollect_hinges_red"] = 15,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
