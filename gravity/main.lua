-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )

-- include the Corona "storyboard" module
local storyboard = require "storyboard"

--table to pass paramentes between scenes without polluting the global namespace!
storyboard.state= {}
storyboard.state.reason=""
storyboard.state.missedObjects=0

_G._w 					= display.contentWidth  		-- Get the devices Width
_G._h 					= display.contentHeight 		-- Get the devices Height
_G.gameScore			= 0								-- The Users score
_G.highScore			= 0								-- Saved HighScore value
_G.numberOfLevels		= 1	
_G.moveX 				= 0
_G.moveY				= 0



-- load menu screen
storyboard.gotoScene( "menu" )

