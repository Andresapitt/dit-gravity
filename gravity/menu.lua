-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "widget" library
local widget = require "widget"

--------------------------------------------

-- forward declarations 
local playBtn

-- 'onRelease' event listener for playBtn
local function onPlayBtnRelease()
	
	-- go to level1.lua scene
	storyboard.gotoScene( "intro", "fade", 500 )
	
	return true	-- indicates successful touch
end



-- Called when the scene's view does not exist:
function scene:createScene( event )

	local group = self.view
	

	-- display a black solid background to cover the whole screen
	local background = display.newRect( 0,0, display.contentWidth, display.contentHeight )
	background:setFillColor( 0,0,0 )
	
	
	-- create/position logo/title image on upper-half of the screen
	local titleLogo = display.newImageRect( "images/BG_main.jpg", 427, 240 )
	titleLogo:setReferencePoint( display.CenterReferencePoint )
	titleLogo.x = display.contentWidth * 0.5
	titleLogo.y = 100
	
	-- create a widget button (which will loads level1.lua on release)
	playBtn = widget.newButton{
		label="Play Now",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		font="Lintel",
		width=154, height=40,
		onRelease = onPlayBtnRelease	-- event listener function
	}
	playBtn:setReferencePoint( display.CenterReferencePoint )
	playBtn.x = display.contentWidth*0.5
	playBtn.y = display.contentHeight - 125
	
	-- all display objects must be inserted into group to be managed by storyboard
	group:insert( background )
	group:insert( titleLogo )
	group:insert( playBtn )
end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )

	-- making sure to remove the texture memory of the previous scene.
	local prior_scene = storyboard.getPrevious()
	storyboard.purgeScene( prior_scene )
	local group = self.view
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view	
end


-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	if playBtn then
		playBtn:removeSelf()	-- widgets must be manually removed
		playBtn = nil
	end

end



function scene:didExitScene( event )

	storyboard.purgeScene( "menu" )
	storyboard.removeScene( "menu" )
end


scene:addEventListener( "didExitScene" )

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene