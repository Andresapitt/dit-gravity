-----------------------------------------------------------------------------------------------------------------------------

local storyboard 		= require( "storyboard" )
-- include Corona's "widget" library
local widget = require "widget"
local scene 			= storyboard.newScene()


local image, againBtn, menuBtn


-- 'onRelease' event listener for againBtn
local function onPlayBtnRelease()
	-- go to level1.lua scene
	storyboard.gotoScene( "level1", "fade", 500 )
	return true	-- indicates successful touch
end

-- 'onRelease' event listener for menuBtn
local function onMenuBtnRelease()
	-- go to level1.lua scene
	storyboard.gotoScene( "menu", "fade", 500 )
	return true	-- indicates successful touch
end



-- Called when the scene's view does not exist:
function scene:createScene( event )
	local screenGroup = self.view

	local background = display.newRect( 0, 0, _G._w , _G._h )
	  	  background:setFillColor( 5,5,7 )
	
	image = display.newImageRect( "images/BG_failed.png",480,320 )
	image.x = display.contentWidth/2
	image.y = display.contentHeight/2

	--display.newText( [parentGroup,] text, x, y, font, fontSize )
	local gameOverText= display.newText( "Game Over", 0,0, "Lintel", 28 )
	gameOverText:setReferencePoint( display.CenterReferencePoint )
	gameOverText.x, gameOverText.y = display.contentWidth/2, display.contentHeight/4

	local reasonCopy=""
	 if storyboard.state.reason=="astronaut" then
	 	reasonCopy="Be careful with you thursters you are now adrift in space"
	 elseif storyboard.state.reason=="objects" then
	 	local object = "objects"
	 	if storyboard.state.missedObjects == 1 then object = "object" end
	 	reasonCopy="You need to be quicker you missed "..storyboard.state.missedObjects .." "..object.."."
	 else
	 	reasonCopy="Look after your oxygen."
	 end

	local reasonText= display.newText( reasonCopy, 0,0, "Lintel", 18 )
	reasonText:setReferencePoint( display.CenterReferencePoint )
	reasonText.x, reasonText.y = display.contentWidth/2, gameOverText.y + gameOverText.height + 30


	-- create a widget button (which will loads level1.lua on release)
	againBtn = widget.newButton{
		label="Play Again",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		font="Lintel",
		fontSize=15,
		width=154, height=40,
		onRelease = onPlayBtnRelease	-- event listener function
	}
	againBtn:setReferencePoint( display.CenterRightReferencePoint )
	againBtn.x = display.contentWidth*0.5 - againBtn.width/2 + 50
	againBtn.y = display.contentHeight - 100

	-- create a widget button (which will loads level1.lua on release)
	menuBtn = widget.newButton{
		label="Back to Menu",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		font="Lintel",
		fontSize=15,
		width=154, height=40,
		onRelease = onMenuBtnRelease	-- event listener function
	}
	menuBtn:setReferencePoint( display.CenterLeftReferencePoint )
	menuBtn.x = display.contentWidth*0.5 + menuBtn.width/2 - 50
	menuBtn.y = display.contentHeight - 100


	screenGroup:insert( background )
	screenGroup:insert( image )
	screenGroup:insert(gameOverText)
	screenGroup:insert(reasonText)
	screenGroup:insert(againBtn)
	screenGroup:insert(menuBtn)
	image.touch = onSceneTouch
end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	storyboard.state.reason=""
	storyboard.state.missedObjects=0
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	local group = self.view
	
	print( "((destroying failed 's view))" )
	
	if againBtn then
		againBtn:removeSelf()	-- widgets must be manually removed
		againBtn = nil
	end

	if menuBtn then
		menuBtn:removeSelf()	-- widgets must be manually removed
		menuBtn = nil
	end

end


function scene:didExitScene( event )
storyboard.purgeScene( "failed" )
storyboard.removeScene( "failed" )
end

scene:addEventListener( "didExitScene" )
---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene