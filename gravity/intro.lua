-----------------------------------------------------------------------------------------------------------------------------

local storyboard 		= require( "storyboard" )
local scene 			= storyboard.newScene()

local widget = require "widget"

-- forward declaration
local backgroundSprite
local starsBG,text1Cont,text2Cont,text3Cont,text4Cont,text5Cont,skipBtn
local textTable
local currentIndex = 1 -- textTabe index/position

local spinningEarth = require("spinning-earth") -- spinning earth sprite sheet information sheet
local spinningEarthSheet = graphics.newImageSheet( "images/spinning-earth.png", spinningEarth:getSheet() )

local howToPlay = require("how-to") -- HUD object sprite sheet
local howToPlaySheet = graphics.newImageSheet( "images/how-to.png", howToPlay:getSheet() )

------------------------------
-- audio ---------------------
local introBgSound = audio.loadStream("audio/mission_abort.mp3")
local introBgSoundChannel 


-- 'onRelease' event listener for skipBtn
local function skipBtnRelease()
	-- go to level1.lua scene
	transition.cancel()
	audio.stop()
	storyboard.gotoScene( "level1", "fade", 500 )
	return true	-- indicates successful touch
end

-- Called when the scene's view does not exist:
function scene:createScene( event )

	local screenGroup = self.view

	textTable = {} -- initialisind the table to store text containers objects

	-- display a black solid background to cover the whole screen
	local background = display.newRect( 0, 0, _G._w , _G._h )
	 background:setFillColor( 0,0,0 )
	
	-- backgroud image of stars
	starsBG = display.newImageRect( "images/intro-bg.png",1136, 640)
	starsBG.x = _G._w /2
	starsBG.y = _G._h /2


	--spinning earth spritesheet
	local spinning_earth_sequenceData =
	{
    start=1, count=50, time=3500 
	}

	backgroundSprite = display.newSprite( spinningEarthSheet, spinning_earth_sequenceData  )
	backgroundSprite.x = _G._w/2
	backgroundSprite.y = _G._h + backgroundSprite.height/2
	backgroundSprite:setReferencePoint(display.BottomCenterReferencePoint)



	-- different texts used
	local text1= "at 372 miles above the Earth"
	local text2 = "the view is breathtaking"
	local text3 = "but life is imposible"
	local text4= "You are floating alone in the vastness of space."
	local text5 = "recover all the pieces to fix your shuttle \n and get back safely to earth."

	-- text containers.
	text1Cont= display.newText( text1 , 0,0, "Lintel", 16 )
	text1Cont:setReferencePoint( display.CenterReferencePoint )
	text1Cont.x, text1Cont.y = display.contentWidth/2, display.contentHeight/2
	text1Cont.xScale, text1Cont.yScale =1, 1
	text1Cont.alpha=0
	table.insert(textTable,text1Cont) 

	text2Cont= display.newText( text2 , 0,0, "Lintel", 16 )
	text2Cont:setReferencePoint( display.CenterReferencePoint )
	text2Cont.x, text2Cont.y = display.contentWidth/2, display.contentHeight/2
	text2Cont.xScale, text2Cont.yScale =1, 1
	text2Cont.alpha=0
	table.insert(textTable,text2Cont) 

	
	text3Cont= display.newText( text3 , 0,0, "Lintel", 16 )
	text3Cont:setReferencePoint( display.CenterReferencePoint )
	text3Cont.x, text3Cont.y = display.contentWidth/2, display.contentHeight/2
	text3Cont.xScale, text3Cont.yScale =1, 1
	text3Cont.alpha=0
	table.insert(textTable,text3Cont) 


	text4Cont= display.newText( text4 , 0,0, "Lintel", 16 )
	text4Cont:setReferencePoint( display.CenterReferencePoint )
	text4Cont.x, text4Cont.y = display.contentWidth/2, display.contentHeight/2
	text4Cont.xScale, text4Cont.yScale =1, 1
	text4Cont.alpha=0
	table.insert(textTable,text4Cont) 

	local textOptions= {
		text = text5,
		x = 0,
		y= 0,
		width=_G._w/1.25,
		height=_G._h/4,
		font ="Lintel",
		fontSize = 16,
		align = "center"
	}

	text5Cont= display.newText( textOptions )
	text5Cont:setReferencePoint( display.CenterReferencePoint )
	text5Cont.x, text5Cont.y = display.contentWidth/2, display.contentHeight/2 + text5Cont.height/2 - 25
	text5Cont.xScale, text5Cont.yScale =1, 1
	text5Cont.alpha=0
	table.insert(textTable,text5Cont) 


	-- how to play spriteSheet
	howToPlaySprite = display.newSprite( howToPlaySheet , {frames={howToPlay:getFrameIndex("left"),
																   howToPlay:getFrameIndex("right"),
																   howToPlay:getFrameIndex("top"),
																   howToPlay:getFrameIndex("bottom"),
																   howToPlay:getFrameIndex("bounds")}})
	howToPlaySprite.x=_G._w/2
	howToPlaySprite.y=_G._h/2
	howToPlaySprite.alpha=0

	skipBtn = widget.newButton{
		label="Skip",
		labelColor = { default={255}, over={128} },
		defaultFile="images/button.png",
		overFile="images/button-over.png",
		font="Lintel",
		width=154, height=40,
		onRelease = skipBtnRelease	-- event listener function
	}
	skipBtn:setReferencePoint( display.CenterLeftReferencePoint )
	skipBtn.x = display.contentWidth - skipBtn.width +5
	skipBtn.y = display.contentHeight - skipBtn.height +10
	

	screenGroup:insert( background )
	screenGroup:insert( starsBG )
	screenGroup:insert(backgroundSprite)
	screenGroup:insert(text1Cont)
	screenGroup:insert(text2Cont)
	screenGroup:insert(text3Cont)
	screenGroup:insert(text4Cont)
	screenGroup:insert(text5Cont)
	screenGroup:insert(howToPlaySprite)
	screenGroup:insert(skipBtn)

end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
		-- remove previous scene's view
	local prior_scene = storyboard.getPrevious()
	storyboard.purgeScene( prior_scene )

	backgroundSprite:setSequence( "orbit" )
	backgroundSprite:play()

	transition.to( backgroundSprite,{y=_G._h , time=15000}  )

	introBgSoundChannel = audio.play( introBgSound, { channel=1, loops=1 }  ) 

	transition.to( text1Cont , { time=4000, xScale=1.2, yScale=1.2, alpha=1, onComplete=chooseObject} )

end


function continueWithLevel1 (currentObject)

	storyboard.gotoScene( "level1" , "crossFade", 500 )
end

----------------------------------------------------
-- after is finished with the last text continues with how to play 
function continueWithHowTo (currentObject)
	transition.to( currentObject , { time=1000, xScale=1.3, yScale=1.3, alpha=0} )
	timer.performWithDelay(8500, function() audio.fadeOut( { channel=1, time=2000 } ) end )
	timer.performWithDelay(4500, changeFrame, 5 )	
end

---------------------------------------
-- how to play sprite sheet - chnages a how to play frame every 4 seconds
function changeFrame(event)
	howToPlaySprite:setFrame( event.count ) -- current howTo frame
	howToPlaySprite.alpha=1
	howToPlaySprite.yScale=1
	howToPlaySprite.xScale=1
	transition.to( howToPlaySprite , { time=3000, xScale=1.05, yScale=1.05} )
	transition.to( howToPlaySprite , { time=1000, xScale=1.2, yScale=1.2, alpha=0, delay=3000} ) 
	if event.count == 5 then
		timer.performWithDelay(5000, continueWithLevel1)
	end
end


function chooseObject ()

	-- get current object from the textTable table
	local currentObject =  textTable[currentIndex]
	currentIndex = currentIndex + 1
	
	-- if we transition thru all the text objects we continue with level 1
	if (currentIndex < #textTable+1) then
		local nextObject =  textTable[currentIndex]
		transitionObject(currentObject,nextObject)	
	else
		continueWithHowTo(currentObject)
	end
end



function transitionObject( currentObject,nextObject)
	transition.to( currentObject , { time=1000, xScale=1.3, yScale=1.3, alpha=0} )
	transition.to( nextObject , { time=4000, xScale=1.2, yScale=1.2, alpha=1,delay=1000, onComplete=chooseObject} )
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	audio.stop()
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )

	if skipBtn then
		skipBtn:removeSelf()	-- widgets must be manually removed
		skipBtn = nil
	end
	
end

function scene:didExitScene( event )
storyboard.purgeScene( "intro" )
storyboard.removeScene( "intro" )
end

scene:addEventListener( "didExitScene" )

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene