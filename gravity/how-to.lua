--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:0f0dfec84f6296213fecd3a124dbea82:d99edae82a355d2e99597b1a0310551d:0a0ed8aea61189300b20932f9872b75b$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- intro-how-to-01@2x
            x=2,
            y=646,
            width=480,
            height=320,

        },
        {
            -- intro-how-to-02@2x
            x=484,
            y=324,
            width=480,
            height=320,

        },
        {
            -- intro-how-to-03@2x
            x=2,
            y=324,
            width=480,
            height=320,

        },
        {
            -- intro-how-to-04@2x
            x=484,
            y=2,
            width=480,
            height=320,

        },
        {
            -- intro-how-to-05@2x
            x=2,
            y=2,
            width=480,
            height=320,

        },
    },
    
    sheetContentWidth = 966,
    sheetContentHeight = 968
}

SheetInfo.frameIndex =
{

    ["left"] = 1,
    ["right"] = 2,
    ["top"] = 3,
    ["bottom"] = 4,
    ["bounds"] = 5,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
