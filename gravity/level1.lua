-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------
display.setStatusBar(display.HiddenStatusBar) -- Hide the status bar

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
--local physicsData = (require "shapedefs").physicsData(1.0)

-- forward declarations and other locals

local physics = require("physics")
local scaleFactor = 1.0
local physicsData = (require "shapesDef").physicsData(scaleFactor) -- shape definition for physical objects (floating objects and astronaut)

local moveX , moveY= 0,0 -- linearvelocity for the astronaut

local starsBG, background, backgroundSprite

local spinningEarth = require("spinning-earth") -- spinning earth sprite sheet information
local spinningEarthSheet = graphics.newImageSheet( "images/spinning-earth.png", spinningEarth:getSheet() )

local objectToCollect, obj_camera, obj_disc, obj_hammer, obj_drill, scoreText, timerPoints
local HUDobjects = require("HUD-objects") -- HUD object sprite sheet
local HUDobjectsSheet = graphics.newImageSheet( "images/HUD-objects.png", HUDobjects:getSheet() )

local objectsSheetInfo = require("objects") -- floating object sprite sheet
local objectSheet

local t 
local groupAstronaut, thrustSprite,thrustSheet,thrustSequenceData,thrustOptions

local timerMyClosure

--astronaut spritesheet
local astronautOptions = {
	   width = 52.5,
	   height = 83,
	   numFrames = 5
	}	

local astronautSheet = graphics.newImageSheet( "images/astronaut.png", astronautOptions )
local astronautSequenceData = {
	   { name = "full", start=1, count=5, time=500, loopCount=1 }
		}

local iss -- international space station graphic
local objectToCollect_camera, objectToCollect_disc, objectToCollect_hammer, objectToCollect_drill
local numberObjectsToCollect, numberObjectsCollected = 0, 0
local jetpackLabel,jetpackLevel, oxygenLabel, oxygenLevel, oxygen,oxygenMask, itemsRemainingText,oxygenGauge
local fuelAvailable  -- how many change of direction available on this level ( not in used for this prototype/ single level)


----------------------------
-- audio
----------------------------
local backgroundMusic = audio.loadStream("audio/bg.mp3")
local backgroundMusicChannel = audio.play( backgroundMusic, { channel=1, loops=-1 }  )  -- play the background music on channel 1, loop infinitely

local breathing = audio.loadStream("audio/breathing.mp3")
local breathingChannel = audio.play( breathing, { channel=2, loops=-1 }  ) -- play the background breathing and heart beat and loop infinitely

local thursterSound = audio.loadStream("audio/thruster.mp3") -- thurster sounds that will be activated on touch events

local collectedSound = audio.loadStream("audio/quick-blip.mp3") -- blip sound that will be played when an object is collected.





------------------------------------------------------
-- Called when the scene's view does not exist:
------------------------------------------------------
function scene:createScene( event )

	local group = self.view
	
	-- sounds volume
	audio.setVolume( 0, { channel=1 } ) -- set the volume on channel 1
	audio.setVolume( 0, { channel=2 } ) -- set the volume on channel 2
	audio.fade( { channel=1, time=4000, volume=0.75 } ) -- fade bacground sounds in
	audio.fade( { channel=2, time=5000, volume=1 } )

	-- table for holding all the object that need to be tested if they are collected or out of bounds of the stage.
	t = {}


	fuelAvailable = 10000 -- how many change of direction available on this level ( in future levels the number of moves can be limited with this)


	-- create a black rectangle as the backdrop
	background = display.newRect( 0, 0, _G._w, _G._h )
	background:setFillColor( black )

	-- backgroud image of stars
	starsBG = display.newImageRect( "images/intro-bg.png",1136, 640)
	starsBG.x = _G._w /2
	starsBG.y = _G._h /2

	--spinning earth spritesheet
	local spinning_earth_sequenceData =
	{
    start=1, count=50, time=3500 
	}
	
	backgroundSprite = display.newSprite( spinningEarthSheet, spinning_earth_sequenceData  )
	backgroundSprite.x = _G._w/2
	backgroundSprite.y = _G._h - backgroundSprite.height/2
	backgroundSprite:setReferencePoint(display.BottomCenterReferencePoint)

	-- international space station
	iss = display.newImage( "images/International-Space-Station.png", 0, 0  )
	iss.x= _G._w + iss.width/2


	--------------------------------------------------------------------------
	-- HUD -> objects to collect ( three frames each: white, missed/red, rescued/normal )
	--------------------------------------------------------------------------
	objectToCollect_disc = display.newSprite( HUDobjectsSheet , {frames={HUDobjects:getFrameIndex("toCollect_disc"),HUDobjects:getFrameIndex("toCollect_disc_red"),HUDobjects:getFrameIndex("disc")}} )
	objectToCollect_hammer = display.newSprite( HUDobjectsSheet , {frames={HUDobjects:getFrameIndex("toCollect_hammer"),HUDobjects:getFrameIndex("toCollect_hammer_red"),HUDobjects:getFrameIndex("hammer")}} )
	objectToCollect_drill = display.newSprite( HUDobjectsSheet , {frames={HUDobjects:getFrameIndex("toCollect_drill"),HUDobjects:getFrameIndex("toCollect_drill_red"),HUDobjects:getFrameIndex("drill")}} )
	objectToCollect_camera = display.newSprite( HUDobjectsSheet , {frames={HUDobjects:getFrameIndex("toCollect_camera"),HUDobjects:getFrameIndex("toCollect_camera_red"),HUDobjects:getFrameIndex("camera")}} )

	objectToCollect = display.newGroup( )

	objectToCollect:insert(objectToCollect_camera)
	objectToCollect:insert(objectToCollect_disc)
	objectToCollect_disc.x=objectToCollect_camera.width+5
	objectToCollect:insert(objectToCollect_hammer)
	objectToCollect_hammer.x=objectToCollect_disc.x+ objectToCollect_disc.width+5
	objectToCollect:insert(objectToCollect_drill)
	objectToCollect_drill.x=objectToCollect_hammer.x + objectToCollect_hammer.width 

	objectToCollect.y=40
	objectToCollect.x = 20

	-------------------------------------------------------------
	-- oxygen indicator
	local oxygenGroup = display.newGroup( )

	gauge = display.newImage( "images/gauge.png", 0,0 )


	oxygenGauge = display.newImage( "images/oxygenGauge.png", 0,0 )

	
	oxygenMask = graphics.newMask( "images/oxygenGaugeMask.png" )

	
	oxygenGauge:setMask( oxygenMask )
	oxygenGauge.maskY=oxygenGauge.maskY+5

	oxygenGroup.x= _G._w  - gauge . width
	oxygenGroup.y = _G._h  - gauge . height 

	oxygenGroup:insert(gauge)
	oxygenGroup:insert( oxygenGauge )



	---------------------------------------------
	-- Astronaut and his jetpack
	---------------------------------------------

	groupAstronaut = display.newGroup()
	groupAstronaut.myId="astronaut"
	groupAstronaut:setReferencePoint( CenterReferencePoint )

	thrustOptions = {
	   width = 150,
	   height = 30,
	   numFrames = 29
	}

	 thrustSheet = graphics.newImageSheet( "images/thrusters.png", thrustOptions )

	thrustSequenceData = {
	   { name = "thrust0", start=1, count=1, time=0,   loopCount=1 },
	   { name = "thrust1", start=1, count=29, time=300, loopCount=1 },
	}

	thrustSprite = display.newSprite( thrustSheet, thrustSequenceData )
	thrustSprite.x = 0
	thrustSprite.y = 0
	thrustSprite:setReferencePoint(display.CenterReferencePoint)


	-- astrounaut sprite

	astronautSprite = display.newSprite( astronautSheet, astronautSequenceData )
	astronautSprite.x = 0
	astronautSprite.y = 0
	astronautSprite.alpha = 0

	astronautSprite:setReferencePoint(display.CenterReferencePoint)


	thrustSprite.x,thrustSprite.y = 0 , 0


	groupAstronaut:insert( thrustSprite )
	groupAstronaut:insert( astronautSprite )
	groupAstronaut.anchorChildren = true

			
	oxygenConsumption = 4 -- oxygen consumtion rate
			
	jetpackShots = 10 -- how many tap to change direction is available
	--jetpackConsumption = jetpackLevel.width/jetpackShots -- fuel consumption rato to animate
		
	itemsRemainingText = display.newText("items remaining: 0/0", 10, 0, "Lintel", 13 )

	scoreText  = display.newText("Score: 0", 10, 0, "Lintel", 18 )
	scoreText:setReferencePoint(display.CenterLeftReferencePoint)
	scoreText.x = itemsRemainingText. x + itemsRemainingText .width +20


			-----------------------------
			-- floating objects ---------
			-----------------------------
			
			objectSheet = graphics.newImageSheet( "images/objects.png", objectsSheetInfo:getSheet() )

			obj_drill = display.newSprite( objectSheet , {frames={objectsSheetInfo:getFrameIndex("drill")}} )
			obj_drill.x, obj_drill.y = 290, 192
			obj_drill.inStage=true
			obj_drill.rescued=false
			obj_drill.myId="drill"
			obj_drill.alpha=0
			table.insert(t,obj_drill) -- add object to table to test if is still in the stage
			 
			obj_camera = display.newSprite( objectSheet , {frames={objectsSheetInfo:getFrameIndex("camera")}} )
			obj_camera.x, obj_camera.y = 309, 100
			obj_camera.inStage=true
			obj_camera.rescued=false
			obj_camera.myId= "camera"
			obj_camera.alpha=0
			table.insert(t,obj_camera) -- add object to table to test if is still in the stage
			 
			obj_disc = display.newSprite( objectSheet , {frames={objectsSheetInfo:getFrameIndex("disc")}} )
			obj_disc.x, obj_disc.y = 250, 72
			obj_disc.inStage=true
			obj_disc.rescued=false
			obj_disc.myId= "disc"
			obj_disc.alpha= 0
			table.insert(t,obj_disc) -- add object to table to test if is still in the stage
			 
			obj_hammer = display.newSprite( objectSheet , {frames={objectsSheetInfo:getFrameIndex("hammer")}} )
			obj_hammer.x, obj_hammer.y = 345, 234
			obj_hammer.inStage=true
			obj_hammer.rescued=false
			obj_hammer.myId="hammer"
			obj_hammer.alpha=0		
			table.insert(t,obj_hammer) -- add object to table to test if is still in the stage
			
			
			groupAstronaut.anchorY,groupAstronaut.anchorX = 0.5, 0.5
			groupAstronaut.x, groupAstronaut.y = _G._w/1.25, _G._h/3

		
		oxygenTimer = timer.performWithDelay( 2000, updateOxygen, 0)

		-- all display objects must be inserted into group
		group:insert( background ) -- black solid
		group:insert(starsBG) -- stars background

		group:insert( backgroundSprite ) -- spinning earth
		backgroundSprite:setSequence( "orbit" )
		backgroundSprite:play()

		group:insert(iss)
		group:insert(obj_drill)
		group:insert(obj_camera)
		group:insert(obj_disc)
		group:insert(obj_hammer)
		group:insert( objectToCollect )


		group:insert(oxygenGroup)


		group:insert(itemsRemainingText)
		group:insert(scoreText)
		

		group:insert( groupAstronaut )
	
end -- ends createScene



--------------------------------------------------------------------------------
-- function that calculates the angle between the astronaut and the touch point.
--------------------------------------------------------------------------------
function angleWhereTouched(x1,y1,x2,y2)
	
	--distance between the astronaut and the touch point (x2-x1, y2-y1)	 
	local deltaY = y2 - y1
	local deltaX = x2 - x1

	local angleInDegrees = (math.atan2( deltaY, deltaX)) -- get angle between the astronaut and the touch point 

	return  math.floor(angleInDegrees*57.2957795) +360 -- convert to degrees

end

----------------------------------------------------------------------
-- oxygen function that controls oxygen level and timer for the level.
----------------------------------------------------------------------
function updateOxygen( event )
		timerPoints= timerPoints-1000
		
		astronautSprite:setSequence( "full" )
		astronautSprite:play()
       	transition.to( oxygenGauge , { time=2000, maskY=oxygenGauge.maskY+oxygenConsumption } )
     
		if event.count == 19 then
			print("Game Over - out of oxygen")
			endMusicCompleted("oxygen")
		end	
end


--------------------------------------------------------
-- event listener to check if the objects left the stage
--------------------------------------------------------
function detectIfGone( event )
		-- first check if the astronaut is still in the stage
		if groupAstronaut.y < _G._h and groupAstronaut.y > 0 and groupAstronaut.x > 0 and groupAstronaut.x < _G._w then

			-- check if the object table still has items, otherwise game over
			if #t >0 then
				--print ("object left: ".. #t)
				for i = #t, 1, -1 do

		        	local object = t[i]

		        	-- check if the object has left the stage
		        	if (object.x - object.width/2) > _G._w or (object.x+object.width/2) < 0 then
		        		object.inStage=false -- mark the object for removal
		        	end

		        	--check if the object has left the stage off the top or bottom
		        	if (object.y > _G._h + object.height/2) or (object.y + object.height/2)  < 0 then
		        		object.inStage=false -- mark the object for removal
		        	end


			        if not object.inStage then   -- Check if object is marked for destruction

			        	updateHUD(object.myId,object.rescued) -- update the HUD with the removed object, mark it red if missed or full colour if collected

			            local child = table.remove(t, i)    -- Remove object from table
			            if child ~= nil then
			                -- Remove from display and nil it
			                child:removeSelf()
			                child = nil
			                --print("object: "..object.myId.. "removed")
			                --print("objects left: "..#t)
			            end
			        end
			
			    end
			else
				--game over because we missed items
				endMusicCompleted("objects")
			end
		else
			-- game over because the astronaut drifted off stage
			endMusicCompleted("astronaut")
		end

	end



-------------------------------------------
-- collition detection
-------------------------------------------

function onCollision( event )

		--make sure that at least one of the objects involved in the collition is the astronaut, otherwise it means that two objects collided together but wasn't collected by the player
        if  event.object1.myId == "astronaut" or event.object2.myId == "astronaut"  then

        	 -- flag what object has been rescued and mark it for removal on the next enterframe
        	 if  event.object1.myId ~= "astronaut" then
        	 	 event.object1.inStage=false
        	 	 event.object1.rescued=true --object1 in the collition is the collected object

        	 else
        	 	 event.object2.inStage=false
        	 	 event.object2.rescued=true --object2 in the collition is the collected object
        	 end

        	 audio.stop(4) -- stop channel 4 to be able to play again.
			local collected = audio.play( collectedSound , {channel=4} ) -- play collection sound fx

        end	

end




-----------------------------------------
--    fuction that moves astronaut 
----------------------------------------
function thrustTap(event) 

			-- check if the touch was right/left of the astronaut to move it in opposite direction	        
        	if event.x < groupAstronaut.x then
  			      moveX=moveX+10
  			    else
  			     	moveX=moveX-10  
        		 end
			-- check if the touch was on top/bottom of the astronaut to move it in opposite direction	 
        		if event.y< groupAstronaut.y then
        		 	moveY=moveY+10
        		 else
        		 	moveY=moveY-10
        	end

        	--calculate que angle between the astronaut and the touch point to rotate the thrusters in that direction
        	local angle= angleWhereTouched(event.x,event.y,groupAstronaut.x,groupAstronaut.y)
        	thrustSprite.rotation=angle

	        -- check if there is fuel to move the character (this is for future levels at the moment fuelAvailable is 9999)
	        if fuelAvailable>0 then
	        	groupAstronaut:setLinearVelocity(moveX, moveY)

	        	fuelAvailable=fuelAvailable-1 -- for future implementation
	        	--transition.to( jetpackLevel, { time=1000, width=jetpackLevel.width-jetpackConsumption,x = jetpackLevel.x - jetpackConsumption / 2 } )
				thrustSprite:setSequence( "thrust1" ) -- play thursters animation
				thrustSprite:play()

				audio.stop(3) -- stop channel 3 to be able to play again.
				local thrusterChannel = audio.play( thursterSound , {channel=3} ) -- play thursters sound fx
	        end	
	return true
end




---------------------------------------------------------------------------------
-- function that updates what object are remaining and which ones to be collected
---------------------------------------------------------------------------------

function updateHUD (id,rescued)

	local points = 0 -- points per object collected
	local frame=2 -- move spritesheet to second frame (red graphic)
	
	if rescued then
		frame = 3 -- if the object was removed from the stage as result of being rescued we then move the spritesheet to the 3rd frame
		numberObjectsCollected=numberObjectsCollected+1 -- to check if we collected all the items for this level
		itemsRemainingText.text="items remaining: "..numberObjectsCollected.."/"..numberObjectsToCollect -- update HUD Legend
	end

	-- which object from the HUD does it need to be updated and how many points the user gets for that object
	if id == "camera" then
		points=0.5
		objectToCollect_camera:setFrame( frame ) --  "2" for the red version and "3" for rescued
	elseif id == "drill" then
		objectToCollect_drill:setFrame( frame ) --  "2" for the red version and "3" for rescued
		points= 0.7
	elseif id == "disc" then
		objectToCollect_disc:setFrame( frame ) --  "2" for the red version and "3" for rescued
		points = 0.5
	elseif id == "hammer" then
		objectToCollect_hammer:setFrame( frame ) --  "2" for the red version and "3" for rescued
		points = 0.8
	end

	
	_G.gameScore = math.floor(_G.gameScore +  points * (timerPoints+1) ) -- score points logic.
	scoreText.text="Score: ".._G.gameScore -- display current points in HUD
	scoreText:setReferencePoint(display.CenterLeftReferencePoint)

end




-------------------------------------------------------------------
-- fadeout music before moving to the next scene
-------------------------------------------------------------------
function endMusicCompleted(reason)

	audio.fadeOut( { channel=1, time=2000 } )
	audio.fadeOut( { channel=2, time=1000 } )

	storyboard.state.reason=reason -- save the reason why the level is complete either because all the objects where collected or character drifted off stage

	Runtime:removeEventListener("enterFrame", detectIfGone) -- remove eventListener to avoid continuing collecting object
	Runtime:removeEventListener( "collision", onCollision ) -- as above.
	-- cancel oxygen timer
	timer.cancel(oxygenTimer)
	--remove tap eventListeners
	Runtime:removeEventListener("tap", thrustTap)
	-- after sound fade out go to the next scene
	timerMyClosure = timer.performWithDelay(2300, gameOverFunctionEnd) 

end


-----------------------------------------------------------------
-- game over or completed
-----------------------------------------------------------------
function gameOverFunctionEnd()

	timer.cancel( timerMyClosure ) -- remove timer

	--stop all audio
	audio.stop()
	

	-- options for storyboard changing scene and passing parameter to next screen
	local options =
	{
		effect = "fade",
		time = 800
	}

	
	storyboard.state.missedObjects=numberObjectsToCollect - numberObjectsCollected

	local won=false
	-- if all the items where rescue go to mission completed otherwise go to failed
	if numberObjectsCollected == numberObjectsToCollect then
		print("complete")
		won=true
	elseif storyboard.state.reason == "oxygen" then
		print("failed out of oxygen")
		won=false
	else
		print("failed objects missed")
		won = false
	end

	if won then
	storyboard.gotoScene( "completed", options )
	else
	storyboard.gotoScene( "failed", options )
	end

end -- ends gameOverFunctionEnd



-------------------------------------------------------
-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	
	local group = self.view

	moveX , moveY= 0,0 -- make sure the initial value for movement is 0 in both directions.
	timerPoints = 20000 -- points base on the timer

	numberObjectsToCollect=#t -- number of objects to collect
	numberObjectsCollected=0 -- number of objects already collected
	itemsRemainingText.text="items remaining: "..numberObjectsCollected.."/"..numberObjectsToCollect --hud initialisation
	
	physics.start() -- start gravity
	physics.setGravity(-0.01, 0.015) -- Set the gravity (a very light gravity)
	physics.setDrawMode( "normal" )  -- overlays collision outlines on normal display objects, can be changed to hybrid to see the shapes

	physics.addBody(obj_drill,  physicsData:get("drill") ) -- assigning the shape "drill" (bounds and properties) defined in shapesDef.lua loaded at the top of this file
	physics.addBody(obj_camera,  physicsData:get("camera") ) -- assigning the shape "camera"
	physics.addBody(obj_disc,  {radius = 15, density = 1, friction = 1}) -- assigning the shape "disc"
	physics.addBody(obj_hammer,  physicsData:get("hammer") ) -- assigning the shape "hammer"

	physics.addBody( groupAstronaut, physicsData:get("astronaut") ) ---- assigning the shape "astrounaut"

	obj_drill:setLinearVelocity(-4, 2) -- set linear velocity to start moving the drill object
	obj_drill.angularVelocity=-3 -- make the object rotate aswell
	obj_camera:setLinearVelocity(-10, -2) -- set linear velocity to start moving the camera object
	obj_camera.angularVelocity=8
	obj_disc:setLinearVelocity(0, -1) -- set linear velocity to start moving the disc object
	obj_disc.angularVelocity = 10
	obj_hammer:setLinearVelocity( -1, 2 )
	obj_hammer.angularVelocity=8


	-- fade in objects
	transition.to( obj_drill, {alpha=1, time= 1000, 100} )
	transition.to( obj_camera, {alpha=1, time= 1000, delay=300} )
	transition.to( obj_disc, {alpha=1, time= 1000, delay=600} )
	transition.to( obj_hammer, {alpha=1, time= 1000, delay=800} )

	-- fade in astronaut
	transition.to( astronautSprite, {alpha=1, time= 1000} )

	-- slide in international space station
	transition.to( iss, {x=_G._w - iss.width/2, time= 22000,transition=easing.outSine } )

	-- add event listeners
	Runtime:addEventListener("tap", thrustTap) -- event listener for when the user taps around the astronaut
	Runtime:addEventListener("enterFrame", detectIfGone) -- enterframe listener to check when the objects leave the stage
	Runtime:addEventListener( "collision", onCollision ) -- collision event to check when the object are collected
	
end


------------------------------------------------
-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view	
end

--------------------------------------------------------------------------------
-- If scene's view is removed
function scene:destroyScene( event )
	 
	local group = self.view
	numberObjectsToCollect=nil
	numberObjectsCollected=nil

	package.loaded[physics] = nil
	physics = nil	
end


function scene:didExitScene( event )
	storyboard.purgeScene( "level1" )
	storyboard.removeScene( "level1" )
end

scene:addEventListener( "didExitScene" )

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene