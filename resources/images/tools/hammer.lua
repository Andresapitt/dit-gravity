-- This file is for use with Corona(R) SDK
--
-- This file is automatically generated with PhysicsEdtior (http://physicseditor.de). Do not edit
--
-- Usage example:
--			local scaleFactor = 1.0
--			local physicsData = (require "shapedefs").physicsData(scaleFactor)
--			local shape = display.newImage("objectname.png")
--			physics.addBody( shape, physicsData:get("objectname") )
--

-- copy needed functions to local scope
local unpack = unpack
local pairs = pairs
local ipairs = ipairs

local M = {}

function M.physicsData(scale)
	local physics = { data =
	{ 
		
		["hammer"] = {
                    
                    
                    
                    
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   29, -30  ,  27, -39  ,  31.5, -34.5  ,  31.5, -30.5  ,  30, -29  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -31, 0  ,  -28, 0  ,  -25.5, 2.5  ,  -26.5, 3.5  ,  -31.5, 2.5  ,  -32.5, 1.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   23.5, -24.5  ,  18.5, -32.5  ,  18.5, -33.5  ,  21.5, -36.5  ,  25, -39  ,  27, -39  ,  29, -30  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -8.5, 34.5  ,  -11, 36  ,  -13, 37  ,  -18.5, 32.5  ,  -21, 30  ,  -12, 28  ,  -11, 28  ,  -8.5, 30.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -8, 12  ,  -9, 12  ,  -17, 9  ,  -12.5, 4.5  ,  -7.5, 10.5  ,  -7.5, 11.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -13, 37  ,  -11, 36  ,  -12, 37  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -15, 38  ,  -18.5, 34.5  ,  -18.5, 32.5  ,  -13, 37  ,  -14, 38  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -26.5, 26.5  ,  -29.5, 22.5  ,  -24.5, 13.5  ,  -24, 14  ,  -15.5, 24.5  ,  -21, 30  ,  -23, 30  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -26, -1  ,  -25.5, -0.5  ,  -25.5, 2.5  ,  -28, 0  ,  -27, -1  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -24.5, 13.5  ,  -31.5, 17.5  ,  -32.5, 16.5  ,  -25.5, 10.5  ,  -24.5, 11.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -11, 36  ,  -8.5, 34.5  ,  -10, 36  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -26.5, 27.5  ,  -26.5, 26.5  ,  -24, 29  ,  -25, 29  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -18, 9  ,  -17, 9  ,  -9, 12  ,  -9.5, 12.5  ,  -19.5, 11.5  ,  -19.5, 10.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -14, 5  ,  -13, 5  ,  -15.5, 7.5  ,  -15.5, 6.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   23, -39  ,  25, -39  ,  21.5, -36.5  ,  21.5, -37.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -15.5, 22.5  ,  -20, 12  ,  -19.5, 11.5  ,  -13.5, 17.5  ,  -13.5, 19.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -31.5, 17.5  ,  -24.5, 13.5  ,  -29.5, 22.5  ,  -31.5, 19.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -26.5, 3.5  ,  -25.5, 4.5  ,  -25.5, 10.5  ,  -32.5, 16.5  ,  -32.5, 3.5  ,  -31.5, 2.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -20, 12  ,  -15.5, 22.5  ,  -15.5, 24.5  ,  -23, 14  ,  -21, 12  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -2.5, -7.5  ,  12.5, -11.5  ,  -4.5, -4.5  ,  -4.5, -5.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -29.5, 22.5  ,  -26.5, 26.5  ,  -29.5, 23.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   0.5, -11.5  ,  12.5, -11.5  ,  -2.5, -7.5  ,  -2.5, -8.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   3.5, -15.5  ,  12.5, -11.5  ,  0.5, -11.5  ,  0.5, -12.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -7.5, 10.5  ,  -12.5, 4.5  ,  -3.5, 5.5  ,  -3.5, 6.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -13.5, 17.5  ,  -19.5, 11.5  ,  -9.5, 12.5  ,  -9.5, 13.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   18.5, -18.5  ,  10.5, -23.5  ,  18.5, -32.5  ,  23.5, -24.5  ,  23.5, -23.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   12.5, -11.5  ,  3.5, -15.5  ,  3.5, -16.5  ,  10.5, -23.5  ,  18.5, -18.5  ,  18.5, -17.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -4.5, -4.5  ,  12.5, -11.5  ,  12.5, -10.5  ,  -3.5, 5.5  ,  -12.5, 4.5  ,  -12.5, 3.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   18.5, -32.5  ,  10.5, -23.5  ,  10.5, -24.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -15.5, 24.5  ,  -24, 14  ,  -23, 14  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -12, 28  ,  -21, 30  ,  -15.5, 24.5  }
                    }
                    
                    
                    
		}
		
	} }

        -- apply scale factor
        local s = scale or 1.0
        for bi,body in pairs(physics.data) do
                for fi,fixture in ipairs(body) do
                    if(fixture.shape) then
                        for ci,coordinate in ipairs(fixture.shape) do
                            fixture.shape[ci] = s * coordinate
                        end
                    else
                        fixture.radius = s * fixture.radius
                    end
                end
        end
	
	function physics:get(name)
		return unpack(self.data[name])
	end

	function physics:getFixtureId(name, index)
                return self.data[name][index].pe_fixture_id
	end
	
	return physics;
end

return M

