--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:0870e6d5eb8e7927b33562f17df3c88b:a7a3ad81a0335fe263172669075fb529:fe771c8da95c3db7c6a6588997771641$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- toCollect_camera
            x=24,
            y=33,
            width=20,
            height=19,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 20,
            sourceHeight = 29
        },
        {
            -- toCollect_camera_red
            x=2,
            y=33,
            width=20,
            height=19,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 20,
            sourceHeight = 29
        },
        {
            -- toCollect_disc
            x=68,
            y=31,
            width=20,
            height=23,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 20,
            sourceHeight = 29
        },
        {
            -- toCollect_disc_red
            x=46,
            y=31,
            width=20,
            height=23,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 20,
            sourceHeight = 29
        },
        {
            -- toCollect_drill
            x=24,
            y=2,
            width=20,
            height=29,

        },
        {
            -- toCollect_drill_red
            x=2,
            y=2,
            width=20,
            height=29,

        },
        {
            -- toCollect_hammer
            x=92,
            y=2,
            width=19,
            height=23,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 19,
            sourceHeight = 29
        },
        {
            -- toCollect_hammer_red
            x=90,
            y=31,
            width=19,
            height=23,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 19,
            sourceHeight = 29
        },
        {
            -- toCollect_hinges
            x=69,
            y=2,
            width=21,
            height=27,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 21,
            sourceHeight = 29
        },
        {
            -- toCollect_hinges_red
            x=46,
            y=2,
            width=21,
            height=27,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 21,
            sourceHeight = 29
        },
    },
    
    sheetContentWidth = 113,
    sheetContentHeight = 56
}

SheetInfo.frameIndex =
{

    ["toCollect_camera"] = 1,
    ["toCollect_camera_red"] = 2,
    ["toCollect_disc"] = 3,
    ["toCollect_disc_red"] = 4,
    ["toCollect_drill"] = 5,
    ["toCollect_drill_red"] = 6,
    ["toCollect_hammer"] = 7,
    ["toCollect_hammer_red"] = 8,
    ["toCollect_hinges"] = 9,
    ["toCollect_hinges_red"] = 10,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
