--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:27072f1fb88c4c9280f7a72edf21148b:5e8a1f658fb0f83b9c775eb09509f4fb:94200563ed9562bb5bde4c1a39ff28d6$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- disc-05
            x=283,
            y=1,
            width=70,
            height=58,

        },
        {
            -- disc-07
            x=211,
            y=1,
            width=70,
            height=75,

        },
        {
            -- disc
            x=1,
            y=1,
            width=69,
            height=99,

        },
        {
            -- hammer
            x=145,
            y=1,
            width=64,
            height=77,

        },
        {
            -- hinges
            x=72,
            y=1,
            width=71,
            height=89,

        },
    },
    
    sheetContentWidth = 354,
    sheetContentHeight = 101
}

SheetInfo.frameIndex =
{

    ["disc-05"] = 1,
    ["disc-07"] = 2,
    ["disc"] = 3,
    ["hammer"] = 4,
    ["hinges"] = 5,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
