--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:61ab841866e4a3ce9a397b31605236a1:3ca5c3f31ed6241938c795a5d52c8419:241676fa3f9697b21b8373ee5692e671$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- Untitled-1_02
            x=1,
            y=1,
            width=69,
            height=100,

        },
        {
            -- Untitled-1_05
            x=283,
            y=1,
            width=70,
            height=56,

        },
        {
            -- Untitled-1_06
            x=145,
            y=1,
            width=70,
            height=75,

        },
        {
            -- Untitled-1_10
            x=217,
            y=1,
            width=64,
            height=74,

        },
        {
            -- Untitled-1_13
            x=72,
            y=1,
            width=71,
            height=89,

        },
    },
    
    sheetContentWidth = 354,
    sheetContentHeight = 102
}

SheetInfo.frameIndex =
{

    ["Untitled-1_02"] = 1,
    ["Untitled-1_05"] = 2,
    ["Untitled-1_06"] = 3,
    ["Untitled-1_10"] = 4,
    ["Untitled-1_13"] = 5,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
