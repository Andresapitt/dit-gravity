local screenW, screenH, halfW = display.contentWidth, display.contentHeight, display.contentWidth*0.5
local circle = display.newCircle( screenW/2, screenH/2, 30 )
--local thrust = display.newImageRect( "thrust-test.png", 83, 40)

local thrustOptions = {
   width = 73,
   height = 40,
   numFrames = 10
}
local thrustSheet = graphics.newImageSheet( "thrust.png", thrustOptions )

local thrustSequenceData = {
   { name = "thrust0", start=1, count=1, time=0,   loopCount=1 },
   { name = "thrust1", start=1, count=10, time=500, loopCount=1 },
}

local thrustSprite = display.newSprite( thrustSheet, thrustSequenceData )
thrustSprite:setReferencePoint(display.CenterReferencePoint)
thrustSprite.x, thrustSprite.y = screenW/2, screenH/4


--local thru = newImageRect( [parentGroup,], filename, [baseDirectory,], width, height )

--thrust:setReferencePoint( display.CenterReferencePoint )
--thrust.x, thrust.y = screenW/2, screenH/2
--thrust.xReference=screenW/2
--thrust.yReference=screenH/2
--thrust.rotation=0

local function doubleTap(event) -- So that you can see the field, just a bonus
		        
		   
        	local x, y = event.x, event.y

        	local dist= getImageRotation(event.x,event.y,thrustSprite.x,thrustSprite.y)*-1
        	print( math.tan(dist) )
        	thrustSprite.rotation = dist
      end


Runtime:addEventListener("tap", doubleTap)


function getImageRotation(x1,y1,x2,y2)
	    --local PI = 3.14159265358
	    local deltaY = y2 - y1
	    local deltaX = x2 - x1

	    local angleInDegrees = (math.atan2( deltaY, deltaX))

	    --local angleInDegrees = math.sqrt( (deltaY - deltaX)  / PI)

	    local mult = 10^0

	    --return math.floor(angleInDegrees * mult + 0.5) / mult
	    return 360 - math.floor(angleInDegrees*57.2957795)
	end