local custom={}

local CBE=require("CBEffects.Library")

function custom.initiate()
	------------------------------------------------------------------------------
	-- Your Code Here
	------------------------------------------------------------------------------
	local mrand=math.random
	local newText=display.newText
	local textString=[[

The custom code option allows you to play around with all of the awesome CBEffects resources from CBResources. Edit as much as you want - simply put your code inside the "initiate" function found in custom.lua.

To load this on startup, change "mode" in main.lua (line #44) to "custom".

To return to the sample browser, change the mode to "samples".

To choose what to load on startup, use "choose" as the mode.

Happy effects!]]

	local myVent=CBE.NewVent{
		title = "wisps",
		build=function()
				local size=math.random(10, 30) -- Particles are a bit bigger than ice comet particles
				return display.newImageRect("Textures/generic_particle.png", size, size)
			end,
		onCreation = function(p)
			p._particlephysics.angularVelocity = mrand(-20, 10)
		end,
		perEmit=1,
		emitDelay=1,
		positionType="atPoint",
		rectLeft=0,
		rectTop=0,
		fadeInTime=50,
		lifeSpan=1500,
			endAlpha=0,
		emitDelay=1,
		physics={
			xDamping=1.0, -- Lose their X-velocity quickly
			gravityY=0.1,
			velocity=0.5
		}
	}

	
	myVent:start()
	
	--local customText = newText(textString, 30, 0, display.contentWidth-30, 0, "Trebuchet MS", 25)
end

return custom